'use strict';

var util = require('util');
var path = require('path');
var fs = require('fs');
var SQLite = require('sqlite3').verbose();
var Bot = require('slackbots');
var schedule = require('node-schedule');


var Baut = function Constructor(settings) {
    this.settings = settings;
    this.settings.name = this.settings.name;
    this.dbPath = settings.dbPath || path.resolve(__dirname, '..', 'data', 'data.db');

    this.user = null;
    this.db = null;
};

util.inherits(Baut, Bot);

Baut.prototype.run = function () {
    Baut.super_.call(this, this.settings);
    this.on('start', this._onStart);
    this.on('message', this._onActivity);
};

Baut.prototype._onStart = function () {
    this._loadBotUser();
    this._connectDb();
    this._firstRunCheck();
    this._startChronJobs();
};

Baut.prototype._loadBotUser = function () {
    var self = this;
    this.user = this.users.filter(function (user) {
        return user.name === self.name;
    })[0];
};

Baut.prototype._connectDb = function () {
    if (!fs.existsSync(this.dbPath)) {
        console.error('Database path ' + '"' + this.dbPath + '" does not exists or it\'s not readable.');
        process.exit(1);
    }
    this.db = new SQLite.Database(this.dbPath);
};

Baut.prototype._firstRunCheck = function () {
    var self = this;
    self.db.get('SELECT val FROM info WHERE name = "lastrun" LIMIT 1', function (err, record) {
        if (err) {
            return console.error('DATABASE ERROR:', err);
        }
        var currentTime = (new Date()).toJSON();
        if (!record) {
            self._welcomeMessage();
            return self.db.run('INSERT INTO info(name, val) VALUES("lastrun", ?)', currentTime);
        }
        self.db.run('UPDATE info SET val = ? WHERE name = "lastrun"', currentTime);
    });
};

Baut.prototype._startChronJobs = function(){
    schedule.scheduleJob('10 11 * * *', function(){
        console.log("11:10a job");
    });
};

Baut.prototype._onActivity = function (message) {
    if (this._isChatMessage(message) && this._isChannelConversation(message) && !this._isFromBaut(message) && this._isMentioningBaut(message))
    {
        this._parseMessage(message);
    }
    if(this._isChatMessage(message) && this._isDirectMessage(message) && !this._isFromBaut(message))
    {
        this._replyToDm(message);
    }
};

Baut.prototype._parseMessage = function(originalMessage) {
    var message = originalMessage.text;
    var query;
    var queryStart = message.indexOf('ghost ') + (this.name.length + 1);
    var queryEnd;
    
    if (message.indexOf("?") > -1){
        queryEnd = message.indexOf("?");
    }
    else if (message.indexOf("!") > -1){
        queryEnd = message.indexOf("!");
    }
    else if (message.indexOf(".") > -1){
        queryEnd = message.indexOf(".");
    }
    else {
        queryEnd = message.length;
    }
    query = message.substring(queryStart,queryEnd);
    this._parseQuery(query);
},

Baut.prototype._parseQuery = function(query){
    console.log("Query received: " + query);
},

Baut.prototype._replyWithLine = function (originalMessage) {
    var self = this;
    var channel = null;
    self.db.get('SELECT id, text FROM quotes ORDER BY last_used ASC, RANDOM() LIMIT 1', function (err, record) {
        if (err) {
            return console.error('DATABASE ERROR:', err);
        }
        channel = self._getChannelById(originalMessage.channel);
        self.postMessageToChannel(channel.name, record.text, {as_user: true});
        self.db.run('UPDATE quotes SET last_used = datetime()  WHERE id = ?', record.id);
    });
};

Baut.prototype._replyToDm = function (originalMessage) {
    var self = this;
    var conversant = originalMessage.user;
    var username = self._getUserName(conversant);
    
    // TODO - write parser for each different parameter that may be passed to bot.
    var runDist = originalMessage.text;
    
    var dateStamp = (new Date()).toJSON().substring(0,10);

    //establish username's privileges 
    

    if(username == 'likefelder') {
        //attempt to find a record from current date and update.
        self.db.get('SELECT * FROM activity WHERE Date LIKE '+ dateStamp +' LIMIT 1', function(err, record){
            if (err) {
                return console.error('DATABASE ERROR:', err);
            }
            else if (record) {    
                self.db.run("UPDATE activity SET Run_Distance = " + runDist + " WHERE Date = ?", dateStamp);
                self.postMessageToUser(conversant, "Found record for " + dateStamp +" and updated it with " + runDist + ".")
            }
            //record was not found, create a new entry
            else if (!record){        
                console.log("Database record for date " + dateStamp + " was not found. Attempting to create new record.");
                self.db.run("INSERT INTO activity (Date, Run_Distance, Run_Duration, Bike_Distance, Bike_Duration, Bike_Duration, Swim_Distance, Swim_Duration, Weight) VALUES (?, '" + runDist + "', '', '', '', '', '' ,'','')", dateStamp);
                self.postMessageToUser(username, "Created new record for " + dateStamp +" and added " + runDist + ".", {as_user:true});}
            })
    }
    if (username != 'likefelder') {
        //self.postMessageToUser(username, ">" + username + "is not authorized to write to the database.", {as_user:true});
        console.log(username + " is not authorized to write to my database.");
    }
    if (username == 'seantheflux'){
        //self.postMessageToUser(username, ">" + username + "is not authorized to write to the database.", {as_user:true});
        console.log(username + " is not authorized to write to my database.");
    }
};

Baut.prototype._welcomeMessage = function () {
},

Baut.prototype._sendMessage = function(channel, message, user){
    console.log("sending message");
    Baut.prototype.postMessageToChannel(channel, message, {as_user: true});
},


Baut.prototype._getChannelById = function (channelId) {
    return this.channels.filter(function (item) {
        return item.id === channelId;
    })[0];
};

Baut.prototype._getUserName = function (userId){
    return this.users.filter(function (item){
        return item.id === userId;
    })[0].name;
}

Baut.prototype._isChatMessage = function (message) { return message.type === 'message' && Boolean(message.text); };
Baut.prototype._isChannelConversation = function (message) { return message.channel[0] === 'C'; };
Baut.prototype._isDirectMessage = function (message) { return message.channel[0] === 'D'; };
Baut.prototype._isMentioningBaut = function (message) { return message.text.toLowerCase().indexOf(this.name) > -1; };
Baut.prototype._isFromBaut = function (message) { return message.user === this.user.id; };

module.exports = Baut;
