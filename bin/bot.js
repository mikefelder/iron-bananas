#!/usr/bin/env node

'use strict';

var Baut = require('../lib/baut');

var token = ''; //gut for development.
var dbPath = process.env.BOT_DB_PATH;
var name = ''; //gut for development

var baut = new Baut({
    token: token,
    dbPath: dbPath,
    name: name
});

baut.run();